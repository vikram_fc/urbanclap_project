package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.example.demo.dto.Service;

@Repository
public interface ServiceRepository extends JpaRepository<Service,Integer>
{

}
