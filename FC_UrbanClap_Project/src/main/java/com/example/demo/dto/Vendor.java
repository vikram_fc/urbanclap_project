package com.example.demo.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the Vendor database table.
 * 
 */
@Entity
@NamedQuery(name="Vendor.findAll", query="SELECT v FROM Vendor v")
public class Vendor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="VendorID")
	private int vendorID;

	@Column(name="CreatedDate")
	private Timestamp createdDate;

	@Column(name="Email")
	private String email;

	@Column(name="FirstName")
	private String firstName;

	@Column(name="IsActive")
	private boolean isActive;

	@Column(name="IsVerified")
	private boolean isVerified;

	@Column(name="LastName")
	private String lastName;

	@Column(name="Mobile")
	private String mobile;

	@Column(name="ModifiedDate")
	private Timestamp modifiedDate;

//	//bi-directional many-to-one association to Service
//	@OneToMany(mappedBy="vendor")
//	private List<Service> services;

//	//bi-directional one-to-one association to Address
//	@OneToMany(cascade = CascadeType.ALL,mappedBy="vendor")
//	private Set<Address> address;

	public Vendor() {
	}

	public int getVendorID() {
		return this.vendorID;
	}

	public void setVendorID(int vendorID) {
		this.vendorID = vendorID;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean getIsVerified() {
		return this.isVerified;
	}

	public void setIsVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

//	public List<Service> getServices() {
//		return this.services;
//	}
//
//	public void setServices(List<Service> services) {
//		this.services = services;
//	}

//	public Service addService(Service service) {
//		getServices().add(service);
//		service.setVendor(this);
//
//		return service;
//	}
//
//	public Service removeService(Service service) {
//		getServices().remove(service);
//		service.setVendor(null);
//
//		return service;
//	}

//	public Set<Address> getAddress() {
//		return this.address;
//	}
//
//	public void setAddress(Set<Address> address) {
//		this.address = address;
//	}

}