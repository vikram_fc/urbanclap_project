package com.example.demo.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * The persistent class for the Service database table.
 * 
 */
@Entity
@NamedQuery(name="Service.findAll", query="SELECT s FROM Service s")
public class Service implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ServiceID")
	private int serviceID;

	@Column(name="CreatedDate")
	private Timestamp createdDate;

	@Column(name="IsActive")
	private boolean isActive;

	@Column(name="ModifiedDate")
	private Timestamp modifiedDate;

	@Column(name="ServiceCost")
	private double serviceCost;

	@Column(name="ServiceUnit")
	private String serviceUnit;

//	//bi-directional many-to-one association to CustomerFeedback
//	@OneToMany(mappedBy="service")
//	private List<CustomerFeedback> customerFeedbacks;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="CategoryID", foreignKey=@ForeignKey(name = "FK_Service_Category"))
	private Category category;

	//bi-directional many-to-one association to Vendor
	@ManyToOne
	@JoinColumn(name="VendorID", foreignKey=@ForeignKey(name = "FK_Service_Vendor"))
	private Vendor vendor;

	public Service() {
	}

	public int getServiceID() {
		return this.serviceID;
	}

	public void setServiceID(int serviceID) {
		this.serviceID = serviceID;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public double getServiceCost() {
		return this.serviceCost;
	}

	public void setServiceCost(double serviceCost) {
		this.serviceCost = serviceCost;
	}

	public String getServiceUnit() {
		return this.serviceUnit;
	}

	public void setServiceUnit(String serviceUnit) {
		this.serviceUnit = serviceUnit;
	}

//	public List<CustomerFeedback> getCustomerFeedbacks() {
//		return this.customerFeedbacks;
//	}
//
//	public void setCustomerFeedbacks(List<CustomerFeedback> customerFeedbacks) {
//		this.customerFeedbacks = customerFeedbacks;
//	}

//	public CustomerFeedback addCustomerFeedback(CustomerFeedback customerFeedback) {
//		getCustomerFeedbacks().add(customerFeedback);
//		customerFeedback.setService(this);
//
//		return customerFeedback;
//	}
//
//	public CustomerFeedback removeCustomerFeedback(CustomerFeedback customerFeedback) {
//		getCustomerFeedbacks().remove(customerFeedback);
//		customerFeedback.setService(null);
//
//		return customerFeedback;
//	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Vendor getVendor() {
		return this.vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

}