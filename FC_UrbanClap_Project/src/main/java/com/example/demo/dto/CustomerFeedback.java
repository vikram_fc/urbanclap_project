package com.example.demo.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the CustomerFeedback database table.
 * 
 */
@Entity
@NamedQuery(name="CustomerFeedback.findAll", query="SELECT c FROM CustomerFeedback c")
public class CustomerFeedback implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="FeedbackID")
	private int feedbackID;

	@Column(name="CreatedDate")
	private Timestamp createdDate;

	@Column(name="Description")
	private String description;

	@Column(name="Rating")
	private short rating;

	//bi-directional many-to-one association to Service
	@ManyToOne
	@JoinColumn(name="ServiceID", foreignKey=@ForeignKey(name = "FK_CustomerFeedback_Service"))
	private Service service;

	public CustomerFeedback() {
	}

	public int getFeedbackID() {
		return this.feedbackID;
	}

	public void setFeedbackID(int feedbackID) {
		this.feedbackID = feedbackID;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public short getRating() {
		return this.rating;
	}

	public void setRating(short rating) {
		this.rating = rating;
	}

	public Service getService() {
		return this.service;
	}

	public void setService(Service service) {
		this.service = service;
	}

}