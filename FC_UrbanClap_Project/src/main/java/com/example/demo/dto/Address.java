package com.example.demo.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;


/**
 * The persistent class for the Address database table.
 * 
 */
@Entity
@NamedQuery(name="Address.findAll", query="SELECT a FROM Address a")
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="AddressID")
	private int addressID;

	@Column(name="Address")
	private String address;

	@Column(name="City")
	private String city;

	@Column(name="Country")
	private String country;

	@Column(name="State")
	private String state;

	@ManyToOne
	@JoinColumn(name="VendorID", nullable=false,foreignKey=@ForeignKey(name = "FK_Address_Vendor"))
	private Vendor vendor;

	public Address() {
	}

	public int getAddressID() {
		return this.addressID;
	}

	public void setAddressID(int addressID) {
		this.addressID = addressID;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public Vendor getVendor() {
		return this.vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}
	
//	public Vendor getVendors() {
//		return vendors;
//	}
//
//	public void setVendors(Vendor vendors) {
//		this.vendors = vendors;
//	}

//	public List<Vendor> getVendors() {
//		return this.vendors;
//	}
//
//	public void setVendors(List<Vendor> vendors) {
//		this.vendors = vendors;
//	}

//	public Vendor addVendor(Vendor vendor) {
//		getVendors().add(vendor);
//		vendor.setAddress(this);
//
//		return vendor;
//	}
//
//	public Vendor removeVendor(Vendor vendor) {
//		getVendors().remove(vendor);
//		vendor.setAddress(null);
//
//		return vendor;
//	}
	
}