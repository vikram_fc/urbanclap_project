package com.example.demo.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;


/**
 * The persistent class for the Category database table.
 * 
 */
@Entity
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CategoryID")
	private int categoryID;

	@Column(name="CategoryName")
	private String categoryName;

	@Column(name="CreatedDate")
	private Timestamp createdDate;

	@Column(name="Description")
	private String description;

	@Column(name="IsActive")
	private boolean isActive;

	@Column(name="ModifiedDate")
	private Timestamp modifiedDate;

	@Column(name="ParentID")
	private int parentID;

//	//bi-directional many-to-one association to Service
//	@OneToMany(mappedBy="category")
//	private List<Service> services;

	public Category() {
	}

	public int getCategoryID() {
		return this.categoryID;
	}

	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public int getParentID() {
		return this.parentID;
	}

	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

//	public List<Service> getServices() {
//		return this.services;
//	}
//
//	public void setServices(List<Service> services) {
//		this.services = services;
//	}
//
//	public Service addService(Service service) {
//		getServices().add(service);
//		service.setCategory(this);
//
//		return service;
//	}
//
//	public Service removeService(Service service) {
//		getServices().remove(service);
//		service.setCategory(null);
//
//		return service;
//	}

}