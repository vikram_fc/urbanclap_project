package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

public interface ServiceInt {
	public String save(Service service);
	public List<Service> getAll();
}
