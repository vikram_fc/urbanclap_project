package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.CustomerFeedbackRepository;
import com.example.demo.dto.CustomerFeedback;

@Service
public class CustomerFeedbackServiceImpl implements CustomerFeedbackServiceInt{

	@Autowired
	CustomerFeedbackRepository feedbackRepository;
	
	@Override
	public String save(CustomerFeedback feedback) {
		feedbackRepository.save(feedback);
		return "Feedback Saved Successfully...";
	}

	@Override
	public List<CustomerFeedback> getAll() {
		return feedbackRepository.findAll();
	}

}
