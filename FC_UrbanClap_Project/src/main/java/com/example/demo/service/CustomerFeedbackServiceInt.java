package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.CustomerFeedback;

public interface CustomerFeedbackServiceInt {
	
	public String save(CustomerFeedback feedback);
	public List<CustomerFeedback> getAll();
}
