package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.VendorRepository;
import com.example.demo.dto.Vendor;

@Service
public class VendorServiceImpl implements VendorServiceInt{

	
	@Autowired
	VendorRepository vendorRepository; 
	
	@Override
	public String save(Vendor vendor) {
		vendorRepository.save(vendor);
		return "Vendor Saved Successfully..";
	}

	@Override
	public List<Vendor> getAll() {
		List<Vendor> vendors=vendorRepository.findAll();
		return vendors;
	}

	@Override
	public Optional<Vendor> getById(int id) {
		Optional<Vendor> vendor=vendorRepository.findById(id);
		return vendor;
	}

	@Override
	public Optional<Vendor> deleteById(int id) {
		Optional<Vendor> vendor=vendorRepository.findById(id);
		vendorRepository.deleteById(id);
		return vendor;
	}

}
