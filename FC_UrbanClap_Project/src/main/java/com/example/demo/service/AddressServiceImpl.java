package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.AddressRepository;
import com.example.demo.dto.Address;
@Service
public class AddressServiceImpl implements AddressServiceInt{

	@Autowired
	AddressRepository addressRepository;

	@Override
	public String save(Address address) {
		addressRepository.save(address);
		return "Save Address Successfully...";
	}

	@Override
	public List<Address> getAll() {
		List<Address> addresses=addressRepository.findAll();
		return addresses;
	}
	
	
}
