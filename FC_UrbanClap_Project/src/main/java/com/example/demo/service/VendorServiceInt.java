package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import com.example.demo.dto.Vendor;

public interface VendorServiceInt {

	public String save(Vendor vendor);
	public List<Vendor> getAll();
	public Optional<Vendor> getById(int id);
	public Optional<Vendor> deleteById(int id);
}
