package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Category;

public interface CategoryServiceInt {
	
	public String save(Category category);
	public List<Category> getAll();
}
