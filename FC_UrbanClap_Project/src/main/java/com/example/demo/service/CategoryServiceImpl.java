package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.CategoryRepository;
import com.example.demo.dto.Category;

@Service
public class CategoryServiceImpl implements CategoryServiceInt{

	@Autowired
	CategoryRepository categoryRepository;
	
	@Override
	public String save(Category category) {
		categoryRepository.save(category);
		return "Category Saved Successfully...";
	}

	@Override
	public List<Category> getAll() {
		List<Category> categories=categoryRepository.findAll();
		return categories;
	}

}
