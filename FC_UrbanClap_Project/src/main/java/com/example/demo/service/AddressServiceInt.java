package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Address;

public interface AddressServiceInt{
	public String save(Address address);
	public List<Address> getAll();
}
