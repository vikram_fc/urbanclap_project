package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.CustomerFeedbackRepository;
import com.example.demo.dao.ServiceRepository;
import com.example.demo.dto.CustomerFeedback;
import com.example.demo.dto.Vendor;
import com.example.demo.response.SuperResponse;
import com.example.demo.service.CategoryServiceInt;
import com.example.demo.service.CustomerFeedbackServiceInt;

@RestController
@RequestMapping("/api/feedback")
public class CustomerFeedbackController {
		
	@Autowired
	CustomerFeedbackServiceInt feedbackService;
	
	@Autowired
	CustomerFeedbackRepository feedbackRepository;
	
	@Autowired
	CategoryServiceInt categoryService;
	
	@Autowired
	ServiceRepository serviceRepository;
	
	@GetMapping("/preload")
	public SuperResponse preload() {
		SuperResponse res = new SuperResponse(true);
		return res;
	}

	@RequestMapping("/")
	public String check() {
		return "Welcome To CustomerFeedback Controller";
	}
	
	
	@PostMapping("/save")
	public SuperResponse saveVendor(@RequestBody CustomerFeedback feedback) {
		SuperResponse res = new SuperResponse(true);
		String message=feedbackService.save(feedback);
		res.addData(feedback.getFeedbackID());
		res.setSuccess(true);
		res.addMessage(message);
		return res;
	}
	
	@GetMapping("/getAll")
	public SuperResponse getAllRecord() {
		SuperResponse res = new SuperResponse(true);
		res.addData(feedbackService.getAll());
		res.setSuccess(true);
		return res;
	}
	
	@GetMapping("/getByID/{id}")
	public SuperResponse getByID(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<CustomerFeedback> feedback=feedbackRepository.findById(id);
		res.addData(feedback);
		res.setSuccess(true);
		return res;
	}
	
	@GetMapping("/deleteByID/{id}")
	public SuperResponse deleteByID(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<CustomerFeedback> feedback= feedbackRepository.findById(id);
		if(feedback.isPresent())
		{
			feedbackRepository.deleteById(id);
			res.addMessage("Delete Vendor Successfully...");
			res.setSuccess(true);
			return res;
		}
		else {
			res.addMessage("Id is invalid");
			res.setSuccess(false);
			return res;
		}
	}
	
	@GetMapping("/update/{id}")
	public SuperResponse update(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<CustomerFeedback> feedback = feedbackRepository.findById(id);
		res.addData(feedback);
		return res;
	}
	
	@PutMapping("/update")
	public SuperResponse updateVendor(@RequestBody CustomerFeedback feedback)
	{
		SuperResponse res = new SuperResponse(true);
		feedbackRepository.save(feedback);
		res.setSuccess(true);
		res.addMessage("Update successfully...");
		return res;
	}
}
