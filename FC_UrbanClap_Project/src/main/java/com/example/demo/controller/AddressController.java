package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.AddressRepository;
import com.example.demo.dto.Address;
import com.example.demo.response.SuperResponse;
import com.example.demo.service.AddressServiceInt;

@RestController
@RequestMapping("/api/address")
public class AddressController {

	@Autowired
	AddressServiceInt addressService;
	
	@Autowired
	AddressRepository addressRepository;

	@GetMapping("/preload")
	public SuperResponse preload() {
		SuperResponse res = new SuperResponse(true);
		return res;
	}

	@RequestMapping("/")
	public String check() {
		return "Welcome To Address Controller";
	}

	@PostMapping("/save")
	public SuperResponse saveAddress(@RequestBody Address address) {
		SuperResponse res = new SuperResponse(true);
		String message = addressService.save(address);
		res.setSuccess(true);
		res.addMessage(message);
		res.addData(address.getAddressID());
		return res;
	}

	@GetMapping("/getAll")
	public List<Address> getAllRecord() {
		return addressService.getAll();
	}

	@GetMapping("/getByID")
	public SuperResponse getByID() {
		SuperResponse res = new SuperResponse(true);

		return res;
	}

	@GetMapping("/deleteByID/{id}")
	public SuperResponse deleteByID(@PathVariable("id") int id) {
		SuperResponse res = new SuperResponse(true);
		Address address = addressRepository.getOne(id);
		System.out.println(address);
		if (!address.equals(null)) {
			addressRepository.delete(address);
			res.addMessage("Delete address Successfully...");
			res.setSuccess(true);
			return res;
		} else {
			res.addMessage("Id is invalid");
			res.setSuccess(false);
			return res;
		}
	}

	@GetMapping("/update/{id}")
	public SuperResponse update(@PathVariable("id") int id) {
		SuperResponse res = new SuperResponse(true);
		Optional<Address> address = addressRepository.findById(id);
		res.addData(address);
		return res;
	}

	@PutMapping("/update")
	public SuperResponse updateAddress(@RequestBody Address address) {
		SuperResponse res = new SuperResponse(true);
		addressRepository.save(address);
		res.setSuccess(true);
		res.addMessage("Update Successfully");
		return res;
	}
}
