package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ServiceRepository;
import com.example.demo.dto.Category;
import com.example.demo.dto.Service;
import com.example.demo.response.SuperResponse;
import com.example.demo.service.CategoryServiceInt;

@RestController
@RequestMapping("/api/service")
public class ServiceController {

	@Autowired
	ServiceRepository serviceRepository;
	
	@Autowired
	CategoryServiceInt categoryService;
	
	@GetMapping("/preload")
	public SuperResponse preload() {
		SuperResponse res = new SuperResponse(true);
		return res;
	}

	@RequestMapping("/")
	public String check() {
		return "Welcome To Service Controller";
	}
	
	@PostMapping("/save")
	public SuperResponse saveVendor(@RequestBody Service service) {
		SuperResponse res = new SuperResponse(true);
		Category category=service.getCategory();
		categoryService.save(category);
		serviceRepository.save(service);
		res.setSuccess(true);
		res.addMessage("Service Saved Successfully...");
		res.addData(service.getServiceID());
		return res;
	}
	
	@GetMapping("/getByID/{id}")
	public SuperResponse getById(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<Service> service=serviceRepository.findById(id);
		res.addData(service);
		return res;
	}
	
	@GetMapping("/getAll")
	public List<Service> getAll()
	{
		return serviceRepository.findAll();
	}
	
	@DeleteMapping("/deleteByID/{id}")
	public SuperResponse delete(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		System.out.println("In the Method");
		serviceRepository.deleteById(id);
		res.addMessage("Service Deleted Successfully...");
		return res;
	}
	
	@PostMapping("/update")
	public SuperResponse update(@RequestBody Service service)
	{
		SuperResponse res = new SuperResponse(true);
		Category category=service.getCategory();
		serviceRepository.save(service);
		res.setSuccess(true);
		res.addMessage("Service Update Successfully...");
		return res;
	}
}
