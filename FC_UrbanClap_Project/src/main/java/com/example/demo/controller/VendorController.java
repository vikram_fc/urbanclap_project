package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.VendorRepository;
import com.example.demo.dto.Address;
import com.example.demo.dto.Vendor;
import com.example.demo.response.SuperResponse;
import com.example.demo.service.AddressServiceInt;
import com.example.demo.service.VendorServiceInt;

@RestController
@RequestMapping("/api/vendor")
public class VendorController {
	
	@Autowired
	VendorServiceInt vendorService;	
	
	@Autowired
	VendorRepository vendorRepository;
	
	@Autowired
	AddressServiceInt addressService;				
	
	@GetMapping("/preload")
	public SuperResponse preload() {
		SuperResponse res = new SuperResponse(true);
		return res;
	}

	@RequestMapping("/")
	public String check() {
		return "Welcome To Address Controller";
	}
	
	@PostMapping("/save")
	public SuperResponse saveVendor(@RequestBody Vendor vendor) {
		SuperResponse res = new SuperResponse(true);
		String message = vendorService.save(vendor);
		res.setSuccess(true);
		res.addMessage(message);
		res.addData(vendor.getVendorID());
		return res;
	}
	
	@GetMapping("/getAll")
	public List<Vendor> getAllRecord() {
		return vendorService.getAll();
	}
	
	@GetMapping("/getByID/{id}")
	public SuperResponse getByID(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<Vendor> vendor = vendorRepository.findById(id);
		res.addData(vendor);
		res.setSuccess(true);
		return res;
	}
	
	@GetMapping("/deleteByID/{id}")
	public SuperResponse deleteByID(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<Vendor> _vendor= vendorService.getById(id);
		if(_vendor.isPresent())
		{
			Optional<Vendor> vendor = vendorService.deleteById(id);
			res.addMessage("Delete Vendor Successfully...");
			res.setSuccess(true);
			return res;
		}
		else {
			res.addMessage("Id is invalid");
			res.setSuccess(false);
			return res;
		}
	}
	
	@GetMapping("/update/{id}")
	public SuperResponse update(@PathVariable("id") int id)
	{
		SuperResponse res = new SuperResponse(true);
		Optional<Vendor> vendor = vendorService.getById(id);
		res.addData(vendor);
		return res;
	}
	
	@PutMapping("/update")
	public SuperResponse updateVendor(@RequestBody Vendor vendor)
	{
		SuperResponse res = new SuperResponse(true);
		String message = vendorService.save(vendor);
		res.setSuccess(true);
		res.addMessage(message);
		return res;
	}
}
