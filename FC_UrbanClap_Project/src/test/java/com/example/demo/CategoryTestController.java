package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.CategoryRepository;
import com.example.demo.dto.Address;
import com.example.demo.dto.Category;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FcUrbanClapProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryTestController {

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

//	@Test
//	public void testAddCategory() {
//		Category category = new Category();
//		category.setCategoryName("IT");
//		category.setDescription("IT service and company..");
//		category.setParentID(121);
//		category.setIsActive(true);
//		category.setCreatedDate(null);
//		category.setModifiedDate(null);
//
//		categoryRepository.save(category);
//
//	}

	@Test
	public void testGetbyID() {
		Optional<Category> category = categoryRepository.findById(1);
		if (category.isPresent()) {
			System.out.println("GetByID  done");
		}
	}

	@Test
	public void testGetAll() {
		System.out.println("GetALL");
		List<Category> category = categoryRepository.findAll();
	}

//	@Test
//	public void testDeleteById()
//	{
//		System.out.println("Delete By ID  Done");
//		categoryRepository.deleteById(1);
//	}

	@Test
	public void testUpdateAddress() {
		Optional<Category> category = categoryRepository.findById(2);
		if (category.isPresent()) {
			Category _category = new Category();
			_category.setCategoryID(category.get().getCategoryID());
			_category.setCategoryName("Tri Tech");
			_category.setDescription("IT service and learning");
			_category.setParentID(122);
			_category.setIsActive(true);
			_category.setCreatedDate(null);
			_category.setModifiedDate(null);
			categoryRepository.save(_category);
			System.out.println("Record Update Successfully");
		} else {
			System.out.println("Invalid Id");
		}
	}
}
