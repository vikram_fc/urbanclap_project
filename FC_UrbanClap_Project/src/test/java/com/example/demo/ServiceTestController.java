package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.AddressRepository;
import com.example.demo.dao.CategoryRepository;
import com.example.demo.dao.ServiceRepository;
import com.example.demo.dao.VendorRepository;
import com.example.demo.dto.Address;
import com.example.demo.dto.Category;
import com.example.demo.dto.CustomerFeedback;
import com.example.demo.dto.Service;
import com.example.demo.dto.Vendor;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FcUrbanClapProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ServiceTestController {

	@Autowired
	private ServiceRepository serviceRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private VendorRepository vendorRepository;
	
	
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {
		
	}
	
//	@Test
//	public void testAddService()
//	{
//		Service service=new Service();
//		service.setCategory(categoryRepository.getOne(2));
//		service.setVendor(vendorRepository.getOne(1));
//		service.setServiceCost(120);
//		service.setServiceUnit("12");
//		service.setIsActive(true);
//		service.setCreatedDate(null);
//		service.setModifiedDate(null);
//		
//		serviceRepository.save(service);
//	}
	
	@Test
	public void testGetServiceList() {
		List<Service> services = serviceRepository.findAll();
		System.out.println("***********");
		System.out.println(services);
	}

//	 @Test
//	 public void testDeleteVendore()
//	 {
//		 serviceRepository.deleteById(1);
//	 }

//	 @Test
//	 public void testDeleteAll()
//	 {
//		 serviceRepository.deleteAll();
//	 }

	@Test
	public void getbyID() {
		vendorRepository.findById(1);
	}
	
	@Test
	public void testUpdateAddress() {
		Optional<Service> _service = serviceRepository.findById(1);
		if (_service.isPresent()) {
		
			Service service=new Service();
			service.setServiceID(_service.get().getServiceID());
			service.setCategory(categoryRepository.getOne(2));
			service.setVendor(vendorRepository.getOne(1));
			service.setServiceCost(150);
			service.setServiceUnit("10");
			service.setIsActive(true);
			service.setCreatedDate(null);
			service.setModifiedDate(null);
			
			serviceRepository.save(service);
			System.out.println("Record Update Successfully");
		} else {
			System.out.println("Invalid Id");
		}
	}

}
