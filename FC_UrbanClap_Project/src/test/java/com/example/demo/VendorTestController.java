package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.AddressRepository;
import com.example.demo.dao.VendorRepository;
import com.example.demo.dto.Address;
import com.example.demo.dto.Service;
import com.example.demo.dto.Vendor;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FcUrbanClapProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VendorTestController {

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

//	@Test
//	public void testAddVendor() {
//
//		Vendor vendor = new Vendor();
//		vendor.setFirstName("Vikram");
//		vendor.setLastName("Shori");
//		vendor.setEmail("vikramshori@gmail.com");
//		vendor.setMobile("7894561230");

//		vendor.setIsActive(true);
//		vendor.setIsVerified(true);
//		vendor.setCreatedDate(null);
//		vendor.setModifiedDate(null);
//		vendorRepository.save(vendor);
//
//	}

	@Test
	public void testGetVendoreList() {
		List<Vendor> vendors = vendorRepository.findAll();
		System.out.println("***********");
		System.out.println(vendors);
	}

//	 @Test
//	 public void testDeleteVendore()
//	 {
//		 vendorRepository.deleteById(1);
//	 }

//	 @Test
//	 public void testDeleteAll()
//	 {
//		 vendorRepository.deleteAll();
//	 }

	@Test
	public void getbyID() {
		vendorRepository.findById(1);
	}
	
	@Test
	public void testUpdateAddress() {
		Optional<Vendor> _vendor = vendorRepository.findById(1);
		if (_vendor.isPresent()) {
		
			Vendor vendor = new Vendor();
			vendor.setVendorID(_vendor.get().getVendorID());
			vendor.setFirstName("Kuldeep");
			vendor.setLastName("Argal");
			vendor.setEmail("vikramshori@gmail.com");
			vendor.setMobile("7894561230");
		
			vendor.setIsActive(true);
			vendor.setIsVerified(true);
			vendor.setCreatedDate(null);
			vendor.setModifiedDate(null);
			vendorRepository.save(vendor);
			System.out.println("Record Update Successfully");
		} else {
			System.out.println("Invalid Id");
		}
	}

}
