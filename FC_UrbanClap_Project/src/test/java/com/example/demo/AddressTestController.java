package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.AddressRepository;
import com.example.demo.dto.Address;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FcUrbanClapProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AddressTestController 
{
	@Autowired
	private AddressRepository addressRepository;
	
	 @Autowired
	  private TestRestTemplate restTemplate;
	 
	 @LocalServerPort
	  private int port;
	  
	  private String getRootUrl() {
	         return "http://localhost:" + port;
	     }
	  
	  @Test
	     public void contextLoads() {

	     }
	  
	 
	
	  
	  @Test
	    public void testCreateAddress() {
	        Address address = new Address();
	        address.setAddress("163 Sudan");
	        address.setCity("Palsia");
	        address.setState("Vita");
	        address.setCountry("Phulkistan");
	        System.out.println(address);
	        addressRepository.save(address);
	        System.out.println("Record Save");
	       // ResponseEntity<Employee> postResponse = restTemplate.postForEntity(getRootUrl() + "/employees", employee, Employee.class);
	        //assertNotNull(postResponse);
	        //assertNotNull(postResponse.getBody());
	    }
	  
	  @Test
	  public void testGetAddressList()
	  {
		  List<Address> addresses=addressRepository.findAll();
		  System.out.println("***********");
		  System.out.println(addresses);
	  }
	  
//	  @Test
//	  public void testDeleteAddress()
//	  {
//		  Optional<Address> address=addressRepository.findById(1);
//		  addressRepository.deleteById(1);
//	  }
	  
	  @Test
	  public void testUpdateAddress()
	  {
		  Optional<Address> address=addressRepository.findById(1);
		  if(address.isPresent())
		  {
			  Address ad=new Address();
			  ad.setAddressID(address.get().getAddressID());
			  ad.setAddress("Prabu nagar");
			  ad.setCity("Ujjain");
			  ad.setCountry("Hindustan");
			  ad.setState("Madhya Pradesh");
			  addressRepository.save(ad);
			  System.out.println("Record Update Successfully");
		  }
		  else
		  {
			  System.out.println("Invalid Id");
			  
		  }
	  }
	  
}
