package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.CustomerFeedbackRepository;
import com.example.demo.dao.ServiceRepository;
import com.example.demo.dto.Category;
import com.example.demo.dto.CustomerFeedback;
import com.example.demo.dto.Vendor;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FcUrbanClapProjectApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerFeedbackTestController {
	
	@Autowired
	private CustomerFeedbackRepository customerFeedbackRepository;	
	
	@Autowired
	private ServiceRepository serviceRepository;
	
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}
	
//	@Test
//	public void testAddCustomerFeedback()
//	{
//		CustomerFeedback feedback=new CustomerFeedback();
//		feedback.setService(serviceRepository.getOne(2));
//		feedback.setDescription("FeedBack====1st");
//		feedback.setRating((short)5);
//		feedback.setCreatedDate(null);
//		customerFeedbackRepository.save(feedback);
//	}
	
	@Test
	public void testGetFeedbackList() {
		List<CustomerFeedback> customerFeedback = customerFeedbackRepository.findAll();
		System.out.println("***********");
		System.out.println(customerFeedback);
	}

//	 @Test
//	 public void testDeletefeedback()
//	 {
//		 customerFeedbackRepository.deleteById(1);
//	 }

//	 @Test
//	 public void testDeleteAll()
//	 {
//		 customerFeedbackRepository.deleteAll();
//	 }

	@Test
	public void getbyID() {
		customerFeedbackRepository.findById(1);
	}
	
	@Test
	public void testUpdateAddress() {
		Optional<CustomerFeedback> customerFeedback = customerFeedbackRepository.findById(1);
		if (customerFeedback.isPresent()) {
			CustomerFeedback feedback = new CustomerFeedback();
		
			feedback.setFeedbackID(customerFeedback.get().getFeedbackID());
			feedback.setService(serviceRepository.getOne(2));
			feedback.setDescription("FeedBack update");
			feedback.setRating((short)4);
			feedback.setCreatedDate(null);
			customerFeedbackRepository.save(feedback);
			System.out.println("Record Update Successfully");
		} else {
			System.out.println("Invalid Id");
		}
	}
}
